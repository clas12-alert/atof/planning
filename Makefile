html : alert.tjp
	mkdir -p html
	tj3 alert.tjp  -o html

pdf : html/ATOF\ Rigid-Flex\ Boards\ Package.html  html/Full\ Overview.html       html/NALU\ Readout\ Board\ Package.html    html/overview_chart.html               html/rigid_flex.html html/Deliveries.html                        html/jlab_board.html           html/NALU\ Readout\ Module\ Package.html   html/Overview.html                     html/Status.html html/Full\ Chart.html                       html/journal_log.html           html/nalu_resourceGraph.html            html/Petiroc2A\ Readout\ Package.html   html/trade_study.html html/full_overview_chart.html              html/Nalu\ Readout\ Board.html   html/nalu_resources.html                 html/ResourceGraph.html
	mkdir -p pdf
	wkhtmltopdf -O Landscape -s Tabloid html/overview_chart.html  pdf/overview_chart.pdf
	wkhtmltopdf -O Landscape -s Tabloid html/trade_study.html     pdf/trade_study.pdf
	wkhtmltopdf -O Landscape -s Tabloid html/jlab_board.html      pdf/jlab_board.pdf
	wkhtmltopdf -O Landscape -s Tabloid html/rigid_flex.html      pdf/rigid_flex.pdf
	wkhtmltopdf  -s A2 html/full_overview_chart.html              pdf/full_overview_chart.pdf
	wkhtmltopdf -O Landscape -s Tabloid html/Status.html          pdf/Status.pdf
	wkhtmltopdf -O Landscape -s Tabloid html/nalu_resources.html  pdf/nalu_resources.pdf
	wkhtmltopdf -O Landscape -s Tabloid html/Nalu\ Readout\ Board.html  pdf/nalu_board.pdf

png :
	mkdir -p images
	wkhtmltoimage html/overview_chart.html       images/overview_chart.png
	wkhtmltoimage html/trade_study.html          images/trade_study.png
	wkhtmltoimage html/jlab_board.html           images/jlab_board.png
	wkhtmltoimage html/rigid_flex.html           images/rigid_flex.png
	wkhtmltoimage html/full_overview_chart.html  images/full_overview_chart.png
	wkhtmltoimage html/Status.html               images/Status.png
	wkhtmltoimage html/nalu_resources.html       images/nalu_resources.png
	wkhtmltoimage html/Nalu\ Readout\ Board.html images/nalu_board.png


freeze : alert.tjp
	mkdir -p html
	tj3 --freeze alert.tjp  -o html
clean :
	rm -rf html
	rm -rf pdf
	rm -rf images


