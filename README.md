ALERT Project using taskjuggler
===============================

Hosted at https://clas12-alert.gitlab.io/atof/planning/

http://taskjuggler.org/tj3/manual/index.html

Building project:

```
make
```

## Latest Files

* [Browse latest PDFs](https://gitlab.com/clas12-alert/atof/planning/-/jobs/artifacts/master/browse/pdf?job=pdfs).
* [Browse latest images (png)](https://gitlab.com/clas12-alert/atof/planning/-/jobs/artifacts/master/browse/images?job=pdfs).

<a href="https://gitlab.com/api/v4/projects/19977899/jobs/artifacts/master/raw/pdf/overview_chart.pdf?job=pdfs">
<img src="https://gitlab.com/api/v4/projects/19977899/jobs/artifacts/master/raw/images/overview_chart.png?job=pdfs" 
     width="600px" />
</a>




## Making pdfs or pngs

```
wkhtmltopdf html/Overview.html  overview.pdf
```

```
wkhtmltoimage html/Overview.html  overview.png
```
